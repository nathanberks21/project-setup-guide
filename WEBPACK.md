# Webpack Usage

Webpack helps speed up the development process by providing a framework and flexibility to do things such as provide a live development server with live refreshing. There are many advantages to using `webpack`. In other projects you may see `grunt` or `gulp`; these generally do the same or similar things as `webpack`, but `webpack` manages to achieve this in a much less verbose way. The learning curve for `webpack` may be a little steeper as the syntax can be daunting.

## Simple HTML, SASS, JavaScript Setup

Install dependencies:

`npm install -D webpack webpack-cli css-loader sass-loader node-sass style-loader html-webpack-plugin copy-webpack-plugin html-loader babel-loader @babel/core @babel/preset-env webpack-dev-server`

Create a `webpack.config.js` file which will hold the `webpack` configuration.

Enter this to setup the config:

```
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

const outputPath =  path.resolve(__dirname, 'dist');

const config = {
  entry: './src/js/index.js',
  output: {
    path: outputPath,
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              includePaths: [ './src/styles/index.scss' ]
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: './src/index.html'
    }),
    new CopyWebpackPlugin([
      { from: './src/images', to: 'images' } 
  ])
  ],
  devServer: {
    contentBase: outputPath,
    watchContentBase: true
  }
}

module.exports = config;
```

This config will look for `index.html`, `.scss`, `.js` files within a `src` directory.

The main `js` file you need to create will be in `src/js/index.js`.

The main entry point for the `scss` file will be `src/styles/index.scss`.

Styles and JavaScript are automatically injected into the `index.html` at build time so you don't need to worry about linking to these files.

## Running a Development Server

`devServer` allows us to start a development environment that serves our files locally. It will automatically watch for changes to the files in the specified directory and refresh the page directly. The default way of viewing this is navigating to [localhost:8080](localhost:8080) in your browser.

This process builds the files in memory to speed up the process, therefore files aren't being built locally and served.

You can add an additional script to the `package.json` file so it now looks like:

```
"scripts": {
  "build": "webpack --mode production",
  "dev": "webpack-dev-server --mode development  --open"
}
```

`npm run dev` will start the dev server and automatically open in your browser.

Changes made to the above files will automatically refresh the browser.

## Creating a `.babelrc` File

Babel is used as part of the toolchain to make more modern JavaScript code backwards compatible with older browsers that may not support certain JavaScript APIs.

We can set this up simply by creating a `.babelrc` file and load in a preset that allows us to use the latest JavaScript.

To do this create a file named `.babelrc` at the root level of your project. Add the below to it and save:

```
{
  "presets": [
    [ "@babel/preset-env" ]
  ]
}
```

This file can be used to configure a lot more, but this is fine for getting started.

## Example

An example has been set up as above within the example folder.

You can copy these folders and run `npm i` and `npm run dev` or `npm run build`.
