const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

const outputPath =  path.resolve(__dirname, 'dist');

const config = {
  entry: './src/js/index.js',
  output: {
    path: outputPath,
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              includePaths: [ './src/styles/index.scss' ]
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: './src/index.html'
    }),
    new CopyWebpackPlugin([
      { from: './src/images', to: 'images' } 
  ])
  ],
  devServer: {
    contentBase: outputPath,
    watchContentBase: true
  }
}

module.exports = config;