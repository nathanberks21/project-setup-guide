# Git Usage

There's a wide variety of benefits of using Git, but one of the most useful is the ability to keep track of all your files and changes over time; allowing you to view a past history of changes and keeping your code secure and available.

**Download**

https://git-scm.com/download and select your OS.

**Sign in to Git locally eg.**

`git config --global user.name "nathan.berks"`

`git config --global user.email nathan_berks@example.com`

`git config --global user.password "p@55w0rd"`

Now when interacting with git you will be validated and have permission to do so.

**The `.gitignore` file**

`.gitignore` allows you to specify certain folders you don't want to be committed to git. This file is very important and should always be included in your projects! Just create a new file called `.gitignore` in the root of your project.

You will want to add `node_modules` to this folder. This will exclude committing thousands of third-party files that are included in your `package.json` file.

Always check what files you are staging with `git status` and add any files you 

**Workflow when committing files:**

`git add` => `git commit -m "Some message about the commit"` => `git push`

**Common Commands**

Command | Example | Description
--- | --- | ---
**Initialisation** ||
`git clone` | `git clone https://gitlab.com/nathanberks21/project-setup-guide.git` | Clone a project into a folder. Once cloned you can `cd` into that folder eg. `cd project-setup-guide`.
**File Change Information** ||
`git status` | `git status` | View changes to the projects (new, deleted, modified etc. files)
`git diff` | `git diff README.md` | View difference between the remote repo and your local files. You can view individual files or every file.
**Branch Control** ||
`git branch` | `git branch` | View a list of all local branches, with the branch you're on hightlighted.
`git checkout` | `git checkout master` | Checkout an existing branch.
`git checkout -b` | `git checkout -b my-branch-name` | Create a new branch. By default you will work on `master`, you will want to checkout a new branch when making changes. If checking out a new branch you need to add `-b`.
**Staging/Adding Files** ||
`git add -u` | `git add -u` |  Stages all files that have previously been committed to the project, ready to be committed
`git add -A` | `git add -A` |  Stages all files, including new files to the project, ready to be committed
`git add` | `git add someFile.js` |  Stages the specified file, ready to be committed
`git reset --` | `git reset --` | Unstages all staged files, incase you accidentally added files you don't want to stage.
`git reset` | `git reset myFile.js` | Unstages a single file, incase you accidentally added a file you don't want to stage.
**Pulling Files** ||
`git pull` | `git pull` | Pull the latest files from the remote to your local branch.
**Committing Files** ||
`git commit` | `git commit -m "Fix some bug"` |  You can check staged files with `git status`, if you're happy you can commit them with a message after `-m`. The message needs to be wrapped in `" "`
**Pushing Files** ||
`git push` | `git push` | Pushes files up to the remote repo
`git push --set-upstream` | `git push --set-upstream origin my-branch-name` | When initially pushing a new branch you will need to create it on the remote repo. 
