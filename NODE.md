# NodeJS Usage

### Why NodeJS?

NodeJS on itself provides server implementations and local system interaction that isn't available with in-browser JavaScript development. NodeJS can be used by writing JavaScript which makes it easier to pick up and learn as you're using the same language for both NodeJS and frontend development.

The installation of NodeJS also provides access to NPM; Node Package Manager. This provides easy access to a whole heap of third-party packages, and you can easily develop and publish your own.

For frontend development you will solely use `npm`. In the past you may see projects using or referencing `bower`. This is another package manager and is generally unused now as `npm` should be more supported with the latest versions.

### Download

https://nodejs.org/en/

## Frontend Development with `npm`

### Initialisation

Initialise your application by running `npm init` from your projects root directory. This will start a walkthrough allowing you to edit default values. You can just press `enter` to skip through and keep all defaults (they can be edited later) or enter them as you wish.

Once complete you will now have a `package.json` file at your root directory level. This file contains your `npm` configuration, and the values that you entered in the previous step. A list of packages you install will be stored here.

### Install Packages

There are two ways to install packages and have them saved to your `package.json`.

`npm i webpack` will install webpack and save it under a `dependencies` heading in your `package.json` file. In older versions of `npm` you would have had to have add `--save` or `-S` to save to `package.json`, but the new version does it automatically.

`npm i webpack -D` will save `webpack` under a `dev-dependencies` heading in your `package.json` file. 

You should always install development tools using `-D`. Therefore, in the above example, `webpack` should be installed with `-D`.

This is because if we were to `npm publish` our project and another user downloaded it, they would only install the list of packages under the `dependencies` heading.

It also gives clear separation as to which files are required at run-time of the code, and which are used as development tools.

Note that installing packages will add an additional file called `package-lock.json`. This file should be committed and helps speed up the install process when cloning the project as well as keeping files the same version when they are installed using `npm i`.

Note that if cloning a project you can run `npm i`; will install all packages listed within the `package.json` file.

### Running Scripts

You will want to automate and shorten some commands that you would run in the command-line to make them more meaningful and easier to remember.

For example, the command `webpack --mode production` will build you project. You can add a script in your `package.json` file under the `scripts` header. By default a `test` script will have been created.

Below I've deleted the `test` script and replaced it with a `build` script.

```
"scripts": {
  "build": "webpack --mode production"
},
```

I can now execute this command by running `npm run build` within the root directory.
